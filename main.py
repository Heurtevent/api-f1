import requests
import json

BASE_URL = "http://ergast.com/api/f1/"
#http://ergast.com/api/f1/current/last/results.json

def getURL(url):
    url = BASE_URL + url
    results = requests.get(url)
    resultconvert = results.json()
    return resultconvert

def drivernationality():
    print("Name of driver?")
    name_pilote = input()
    print("The nationality of {}".format(name_pilote))
    data = getURL("drivers/{}.json".format(name_pilote)) 
    data_filter = data['MRData']['DriverTable']['Drivers'][0]['nationality']
    print(data_filter)

def birthdriver():
    print("Name of driver?")
    name_pilote = input()
    print("The date of birth of {}".format(name_pilote))
    data = getURL("drivers/{}.json".format(name_pilote)) 
    data_filter = data['MRData']['DriverTable']['Drivers'][0]['dateOfBirth']
    print(data_filter)

def codedriver():
    print("Name of driver?")
    codeofdriver = input()
    print("The code of {}".format(codeofdriver))
    data = getURL("drivers/{}.json".format(codeofdriver)) 
    data_filter = data['MRData']['DriverTable']['Drivers'][0]['code']
    print(data_filter)

def resultlastrace():
    print("Here is the last race ->")
    data = getURL("current/last/results.json")
    date_lastracename= data['MRData']['RaceTable']['Races'][0]['date']
    name_circuit = data['MRData']['RaceTable']['Races'][0]['Circuit']['circuitName']
    namefirst_position = data['MRData']['RaceTable']['Races'][0]['Results'][0]['Driver']['driverId']
    namefirst_constructor = data['MRData']['RaceTable']['Races'][0]['Results'][0]['Constructor']['constructorId']
    print("The first driver is {} with the {} sure the circuit {} the {} ".format(namefirst_position, namefirst_constructor, name_circuit, date_lastracename))

def DataConstructor():
    print("What data of constructur you want know?")
    nameconstructor = input()
    data = getURL("constructors/{}.json".format(nameconstructor))
    name_constructor = data['MRData']['ConstructorTable']['Constructors'][0]['name']
    nationality_constructor = data ['MRData']['ConstructorTable']['Constructors'][0]['nationality']
    print("The nationality of {} is {} ".format(name_constructor,nationality_constructor))

print("What you need? 1-Select A for know the nationality of driver. 2-Select B for date of birth of driver. 3-Select C for know the code of driver. 4-Select D for results of last race. 5-Select E for know the date of constructor")
select = input()
mycase = {
    'a' : drivernationality,
    'b' : birthdriver,
    'c' : codedriver,
    'd' : resultlastrace,
    'e' : DataConstructor
}

myfunc = mycase[select]
myfunc()

